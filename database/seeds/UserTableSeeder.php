<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => ' Victor',
            'email' => 'mvguevara@misena.edu.co',
            'role' => 'ADMIN',
            'password' => bcrypt('123'),
        ]);
    }
}
