@extends('layouts.master')
@section('sidebar')
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                    {{Auth::User()->name}}
                                </strong>
                            </span>
                            <span class="text-muted text-xs block">
                                {{Auth::User()->role}}
                                <b class="caret"></b>
                            </span>
                        </span>
                    </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ URL::to('logout') }}">Salir</a></li>
                        <li class="divider"></li>
                    </ul>
                </div>
                <div class="logo-element">
                    CCM+
                </div>
            </li>
            <li>
                <a href="{{ URL::to('admin') }}">
                    <i class="fa fa-dashboard"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>

            <li class="">
                <a href="{{ URL::to('admin/job-list') }}">
                    <i class="fa fa-sliders"></i>
                    <span class="nav-label">Trabajos</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ URL::to('admin/job-list') }}" >
                            Lista
                            <span class="label label-primary pull-right">
                                <i class="fa fa-bars"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/job/post-new-job') }}">
                            Nuevo Trabajo
                            <span class="label label-primary pull-right">NEW</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/job/search-job') }}" >
                            Buscar Trabajo
                            <span class="label label-primary pull-right">
                                <i class="fa fa-search"></i>
                            </span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class="">
                <a href="{{ URL::to('admin/department-list') }}">
                    <i class="fa fa-sliders"></i>
                    <span class="nav-label">Departamentos</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ URL::to('admin/department-list') }}" >
                            Lista
                            <span class="label label-primary pull-right">
                                <i class="fa fa-bars"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ URL::to('admin/department/add-new-department') }}">
                          Nuevo Departamento
                      <p><span class="label label-primary pull-right">NEW</span></a></p>
                    </li>
                </ul>
            </li>


            <li class="">
                <a href="{{ URL::to('admin/application-list') }}">
                    <i class="fa fa-sliders"></i>
                    <span class="nav-label">Aplicaciones</span> <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ URL::to('admin/application-list') }}" >
                            Lista
                            <span class="label label-primary pull-right">
                                <i class="fa fa-bars"></i>
                            </span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ URL::to('admin/application-search') }}" >
                            Buscar Application
                            <span class="label label-primary pull-right">
                                <i class="fa fa-search"></i>
                            </span>
                        </a>
                        <a href="{{ URL::to('admin/job/application-to-job/{id}') }}">Crear
                        <span class="label label-primary pull-right">NEW</span></a>
                    </li>
                </ul>
            </li>




        </ul>
    </div>
</nav>
@endsection