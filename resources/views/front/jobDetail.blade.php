@extends('layouts.frontEndMaster')
@section('content')
    <div class="col-lg-9 blog-main">

          <div class="blog-post">
            <h3>{{ $jobPost->jobTitle }}</h3>
            <pre><code>Fecha : {{ $jobPost->deadline }} | Departamento : {{ $jobPost->department->departmentName }}</code></pre>
            <p>{{ $jobPost->jobDescription }}</p>
            <nav>
                <ul class="pager">
                  <li><a href="{{ URL::to('/apply-to-job/'.$jobPost->autoGeneratedJobId) }}">APLICACION</a></li>
                  <li><a href="#">LEER MUCHO MAS</a></li>
                </ul>
             </nav>
          </div>
    </div>
@endsection